---
title: "Český rozhlas představuje kandidáty do krajských zastupitelstev"
perex: "Kdo bude vaším příštím hejtmanem? Regionální stanice Českého rozhlasu nabídly všem jedničkám na kandidátkách stejný prostor a možnost odpovědět na stejné otázky; celkem oslovily 269 kandidátů. Poslechněte si jejich odpovědi a rozhodněte se, komu dáte svůj hlas ve volbách 7. a 8. října."
description: "Kdo bude vaším příštím hejtmanem? Regionální stanice Českého rozhlasu nabídly všem jedničkám na kandidátkách stejný prostor a možnost odpovědět na stejné otázy; celkem oslovily 269 kandidátů. Poslechněte si jejich odpovědi a rozhodněte se, komu dáte svůj hlas ve volbách 7. a 8. října."
authors: ["Regionální stanice ČRo", "Anna Kottová", "Kristina Roháčková", "Marcel Šulek"]
published: "8. září 2016"
socialimg: https://interaktivni.rozhlas.cz/data/volby-2016-vizitky-kraje/www/media/mapa.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "lidri-krajskych-kandidatek"
libraries: [d3, topojson]
recommended:
  - link: https://interaktivni.rozhlas.cz/kandidati-do-senatu/
    title: Český rozhlas představuje kandidáty do Senátu
    perex: Šest otázek a stejný časový limit. Český rozhlas představuje všechny kandidáty do Senátu, kteří se rozhodli využít nabídku na natočení takzvaných předvolebních vizitek.
    image: https://interaktivni.rozhlas.cz/data/volby-2016-vizitky/www/media/mapa.png
  - link: https://interaktivni.rozhlas.cz/krajske-kandidatky/
    title: Chybí budoucí hejtmanky, nastupují protisystémové strany: prohlédněte si krajské kandidátky v grafech
    perex: Sociální demokraté postaví do voleb překvapivě mnoho žen, ale žádnou lídryni. ANO bude mít kandidátky téměř stoprocentně z politických nováčků a křesťanští demokraté jsou nejvzdělanější. Co o stranách říkají kandidátky?
    image: https://interaktivni.rozhlas.cz/data/kandidatky-2016-text/www/media/socimg.png
  - link: https://interaktivni.rozhlas.cz/yusra/
    title: Yusra plave o život: Cesta syrské uprchlice z Damašku do olympijského Ria
    perex: Uprchlická celebrita, olympijská plavkyně. Osmnáctiletá Yusra Mardini je nyní hvězdou v záři reflektorů. V srpnu 2015 však byla na útěku. Uprchla se svou sestrou Sarah hledat nový domov. Reportérka Magdalena Sodomková a fotograf Lam Duc Hien zachytili její putování přes Egejské moře do Evropy.
    image: https://interaktivni.rozhlas.cz/yusra/media/fb2.jpg
---

<div class="ig" data-ig="kraje"></div>

Co nabízejí lídři kandidátek vašemu kraji? Koho by si jejich strana zvolila za koaličního partnera? A jak by řešili nejožehavější místní problémy? Možnost odpovědět na otázky Českého rozhlasu dostal před krajskými volbami lídr kandidátky každé strany, hnutí či koalice. Někteří tuto nabídku odmítli, jiní ji přenechali jinému kandidátovi stejného subjektu.

Podobný prostor nabídl Český rozhlas také [všem kandidátům v senátních volbách](https://interaktivni.rozhlas.cz/kandidati-do-senatu/). 

Podoba volebních vizitek je pro kandidáty jednotná. První otázka zní „Proč by měli lidé volit právě vaši stranu/hnutí/koalici?“ a na odpověď mají všichni jednu minutu. Časový limit u dalších dvou otázek, které jsou společné pro všechny kraje, je 30 sekund. Kandidáti můžou odpovědět v kratším čase, ušetřené sekundy se ale nepřevádějí pro další otázky.

Regionální stanice Českého rozhlasu se dále ptají:

- Pokud vyhrajete volby, co uděláte ve vedení kraje jako první?
- Kdo je pro vás nejpřijatelnější koaliční partner?

Další tři otázky směřovaly regionální stanice na konkrétní krajské téma. Pro jejich zodpovězení mají kandidáti opět časový limit 30 sekund.

Podmínky pro všechny kandidáty do krajských zastupitelstev byly stejné. Všichni byli dopředu seznámeni s podobou otázek, při natáčení ale museli odpovídat z hlavy a na první pokus. Nemohli tedy před sebou mít připravený text ani poznámky na papíře nebo v notebooku, tabletu či mobilu. Zhruba 20 kandidátů nabídku na natočení volební vizitky odmítlo.

Rozhovory s jednotlivými kandidáty se ve vysílání regionálních stanic Českého rozhlasu objeví postupně, a to v časech, které určí ředitel regionálních studií. Pořadí určil los, na který dohlížel notář.