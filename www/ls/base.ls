new Tooltip!watchElements!
let
  return unless ig.containers.kraje
  container = d3.select ig.containers.kraje
  kraje = d3.csv.parse ig.data.kraje
  neschopnyKokotiJsouV = {}
  for jouda, index in kraje
    if !jouda.foto
      jouda.foto = "muz.jpg"
    jouda.index = index + 1
  mapka = new ig.Mapka container, {kraje: ig.data.krajeTopo}, neschopnyKokotiJsouV
    ..draw \kraje
    ..on \kraj (kraj) ->
        container.classed \is-share 0
        invitation.remove!
        kraj .= toString!
        str = 
         | kraj == "Kraj Vysočina" => kraj
         | otherwise => "#{kraj} kraj"
        vizitky.display do
          \kraje
          "#{kraj} kraj"
          -> 0 <= it.kraj.indexOf kraj

    
  vizitky = new ig.Vizitky container, {kraje}
  # vizitky.display \kraje "Senátní obvod 13 – Tábor" (.obvod == "1")
  invitation = container.append \div
    ..attr \class \invitation
    ..html "Vyberte si kraj z mapy nebo z menu vpravo nahoře, poté se zde zobrazí seznam lídrů všech kandidátek"
  if window.location.hash and window.location.hash[1] == "k"
    id = parseInt do
      window.location.hash.slice 2
    if kraje[id - 1]
      vizitky.display do
        \kraje
        "Kandidát do krajského zastupitelstva #{that.jmeno} #{that.prijmeni}, #{that.kraj}"
        -> it is that
      container.classed \is-share 1

let
  return unless ig.containers.base
  container = d3.select ig.containers.base
  senat = d3.csv.parse ig.data.senat
  senatAssoc = {}
  for jouda in senat
    senatAssoc["#{jouda.obvod}-#{jouda.cislo}"] = jouda
  mapka = new ig.Mapka container, {senat: ig.data.senatTopo}
    ..draw \senat
    ..on \obvod (obvod, nazev) ->
        container.classed \is-share 0
        invitation.remove!
        obvod .= toString!
        vizitky.display do
          \senat
          "Senátní obvod #{obvod} – #{nazev}"
          -> it.obvod == obvod

    
  vizitky = new ig.Vizitky container, {senat}
  # vizitky.display \senat "Senátní obvod 13 – Tábor" (.obvod == "1")
  invitation = container.append \div
    ..attr \class \invitation
    ..html "Vyberte si volební obvod z mapky nebo z roletky vpravo nahoře, poté se zde zobrazí seznam všech kandidujících senátorů."
  if window.location.hash and window.location.hash[1] != "k"
    id = window.location.hash.slice 1
    if senatAssoc[id]
      vizitky.display do
        \senat
        "Kandidát do senátu #{that.cislo}. #{that.jmeno} #{that.prijmeni}, obvod #{that.obvod_nazev}"
        -> it is that
      container.classed \is-share 1
